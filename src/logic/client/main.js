import { startTracking } from "ubports-astro-components/js/mtm-loader.js";

import DarkMode, {
  setDefaultColorMode
} from "ubports-astro-components/js/darkMode.js";

import HeaderContainer from "ubports-astro-components/js/header.js";
import ToggleButton from "ubports-astro-components/js/toggleButton.js";

export default function registerComponents() {
  // Components
  customElements.define("dark-mode-switch", DarkMode);
  customElements.define("header-container", HeaderContainer);
  customElements.define("toggle-button", ToggleButton);
  startTracking("https://analytics.ubports.com/", "5");
}
